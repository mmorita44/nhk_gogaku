# coding: utf-8

def save_file_dest_dir
  # ダウンロードしたmp3ファイルの保存先を指定する設定。
  # このスクリプト群と同じフォルダ/ディレクトリで良ければ、
  # endの直上の'.'をそのまま（'.'）にしておいてください。
  # 保存先を指定する場合には'.'の「.」部分にファイルパスを書いてください。
  #
  # Macでの例（adminの代わりにご自身の利用ユーザ名を入れてください）
  # '/Users/admin/Music/NHK語学講座'
  # Windows XPでの例（Administratorの代わりにご自身の利用ユーザ名を入れてください）
  #'C:\Documents and Settings\Administrator\My Documents\My Music\NHK語学講座'
  # Windows Vista/7/8での例（同上）
  #'C:\Users\Administrator\Music\NHK語学講座'
  '.'
end

def audio_format
  # 保存したいファイル形式を下の行の4種類から3文字で指定してください。
  # mp3 / m4a / wma / ogg
  mp3
  # ↑初期設定はmp3。書き方は引用符付きの"mp3"や'mp3'でもOKです。
end

def pref
  # ダウンロードしたい語学講座をfalseからtrueへ変更してください。
  {
    "基礎英語1"                => false,
    "基礎英語2"                => false,
    "基礎英語3"                => false,
    "ラジオ英会話"             => false,
    "英会話タイムトライアル"   => false,
    "攻略！英語リスニング"     => false,
    "入門ビジネス英語"         => true,
    "実践ビジネス英語"         => true,
    "英語で読む村上春樹"       => false,
    "エンジョイ・シンプル・イングリッシュ" => false,
    "まいにち中国語"           => false,
    "レベルアップ中国語"       => false,
    "まいにちハングル講座"     => false,
    "レベルアップハングル講座" => false,
    "まいにちイタリア語"       => false,
    "まいにちドイツ語"         => false,
    "まいにちフランス語"       => false,
    "まいにちスペイン語"       => false,
    "まいにちロシア語"         => false
  }
end

# 以下は変更しないでください。
def mp3; :mp3; end
def m4a; :m4a; end
# def aac; :aac; end
def wma; :wma; end
def ogg; :ogg; end


# coding: utf-8

def subject_urls
  {
    :basic1 =>     "english/basic1",
    :basic2 =>     "english/basic2",
    :basic3 =>     "english/basic3",
    :kaiwa =>      "english/kaiwa",
    :timetrial =>  "english/timetrial",
    :kouryaku =>   "english/kouryaku",
    :business1 =>  "english/business1",
    :business2 =>  "english/business2",
    :yomu =>       "english/yomu",
    :enjoy =>      "english/enjoy",
    :chinese =>    "chinese/kouza",
    :chinese_levelup => "chinese/levelup",
    :hangeul =>    "hangeul/kouza",
    :hangeul_levelup => "hangeul/levelup",
    :italian =>    "italian/kouza",
    :german =>     "german/kouza",
    :french =>     "french/kouza",
    :spanish =>    "spanish/kouza",
    :russian =>    "russian/kouza"
  }
end

def subjects_jp
  {
    "基礎英語1"                => :basic1,
    "基礎英語2"                => :basic2,
    "基礎英語3"                => :basic3,
    "ラジオ英会話"             => :kaiwa,
    "英会話タイムトライアル"   => :timetrial,
    "攻略！英語リスニング"     => :kouryaku,
    "入門ビジネス英語"         => :business1,
    "実践ビジネス英語"         => :business2,
    "英語で読む村上春樹"       => :yomu,
    "エンジョイ・シンプル・イングリッシュ" => :enjoy,
    "まいにち中国語"           => :chinese,
    "レベルアップ中国語"       => :chinese_levelup,
    "まいにちハングル講座"     => :hangeul,
    "レベルアップハングル講座" => :hangeul_levelup,
    "まいにちイタリア語"       => :italian,
    "まいにちドイツ語"         => :german,
    "まいにちフランス語"       => :french,
    "まいにちスペイン語"       => :spanish,
    "まいにちロシア語"         => :russian
  }
end

def audio_formats
{ :mp3 => {:ext => '.mp3', :audio_output_option => '-ab 64k'},
  :m4a => {:ext => '.m4a', :audio_output_option => '-absf aac_adtstoasc -acodec copy'},
# :aac => {:ext => '.aac', :audio_output_option => '-acodec copy'},
  :wma => {:ext => '.wma', :audio_output_option => '-ab 64k'},
  :ogg => {:ext => '.ogg', :audio_output_option => '-aq 0 -acodec libvorbis'}}
end
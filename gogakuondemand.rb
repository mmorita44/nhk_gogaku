#!/usr/bin/env ruby
# coding: utf-8

# 全講座DLで約20分、約557MB（mp4からmp3への変換はPC能力に依存）

require 'rexml/document'
require 'net/https'
require 'kconv'

$stdout.sync = true

def check_ffmpeg_has_(regex)
  status = IO.popen('ffmpeg -version 2>&1') do |pipe|
    pipe.read
  end
  status[regex]
end

def is_win?
  RUBY_PLATFORM.downcase[/mswin(?!ce)|mingw|cygwin|bccwin/] ? true : false
end

def load_pref
  set_script_dir
  load_pref_file
  load_subjects_file
  set_save_file_dest_dir
end

def set_script_dir
  @script_dir = Dir.pwd
  push_dir(@script_dir)
end

def load_pref_file
  load File.dirname(__FILE__) + '/pref.rb'
  @select_subjects_jp = []
  pref.each do |subject_jp, bool|
    @select_subjects_jp << subject_jp if bool
  end
  @audio_format = audio_format.to_sym
end

def load_subjects_file
  load File.dirname(__FILE__) + '/subjects.rb'
  set_audio_format(audio_formats)
  @select_subjects = []
  @subject_urls = subject_urls
  @subjects_jp  = subjects_jp
  @select_subjects_jp.each do |sub_jp|
    @select_subjects << @subjects_jp[sub_jp]
  end
end

def set_audio_format(formats)
  @ext                  = formats[@audio_format][:ext]
  @audio_output_option  = formats[@audio_format][:audio_output_option]
  if ARGV[0]
    key = formats.keys.find { |ky| ARGV[0].downcase[/#{ky}/] }
    @ext                  = formats[key][:ext]
    @audio_output_option  = formats[key][:audio_output_option]
  end
end

def set_save_file_dest_dir
  working_dir = File.expand_path(File.dirname(save_file_dest_dir || '.'))
  Dir.mkdir(working_dir) unless File.directory?(working_dir)
  push_dir(working_dir)
end

def print_download_subjects
  download_subjects = @select_subjects_jp.join("、")
  puts "ダウンロードする語学講座：#{download_subjects}"
  puts "ファイルフォーマット：#{@ext.sub(/\./, "")}"
end

def https_body(url)
  uri = URI.parse(url)
  https = Net::HTTP.new(uri.host, uri.port)
  https.use_ssl = true
  https.ssl_version = :TLSv1 if RUBY_VERSION >= "1.9"
  https.verify_mode = OpenSSL::SSL::VERIFY_NONE
  if uri.query
    https.get("#{uri.path}?#{uri.query}").body
  else
    https.get(uri.path).body
  end
end

def get_source_urls
  print "ダウンロード先確認中.."
  data_hash = {}
  @select_subjects.each do |sub|
#    url = "https://cgi2.nhk.or.jp/gogaku/#{@subject_urls[sub]}/listdataflv.xml" # until 2014/3
    url = "https://cgi2.nhk.or.jp/gogaku/st/xml/#{@subject_urls[sub]}/listdataflv.xml"
    doc = REXML::Document.new(https_body(url))
    data_array = doc.get_elements('/musicdata/music')
    date = data_array.map { |d| 
      d.attribute('hdate').to_s.match(/(\d+)月(\d+)日/).captures 
    }.map { |dt| 
      "#{record_year(dt[0].to_i)}_%02d_%02d" % dt 
    }
    data_hash[sub] = {:kouza => data_array[0].attribute('kouza').to_s}
    data_array.each_with_index do |d, i|
      data_hash[sub][date[i]] = data_array[i].attribute('file').to_s
    end
    print ".."
  end
  puts "完了"
  data_hash
end

def record_year(record_month)
  now = Time.now
  this_month = now.month
  this_year = now.year
  @record_year = this_month - record_month < 0 ? this_year - 1 : this_year
end

def prepare_download_each_subject(date_filenames)
  @subject_jp = date_filenames.delete(:kouza)
  puts "\n講座名：#{@subject_jp}"
  subject_dir = File.join(current_dir, "#{@subject}/")
  Dir.mkdir(subject_dir) unless File.directory?(subject_dir)
  subject_dir
end

def each_date_process(date_filenames)
  date_filenames.each do |date, filename|
    @metadata = {}
    @metadata[:subject]   = @subject_jp
    @metadata[:title]     = "#{@subject}_#{date}"
    @metadata[:title_jp]  = "#{@subject_jp}_#{date}"
    @metadata[:genre]     = "Speech"
    @metadata[:create]    = "NHK"
    @metadata[:year]      = @record_year.to_s
    puts "日付：#{date}"
    download_ondemand_file(filename)
  end
end

def downloaded?(pathname)
  File.exists?(pathname) || File.exists?(pathname.tosjis)
end

def download_ondemand_file(filename)
  master_m3u8 = "https://nhk-vh.akamaihd.net/i/gogaku-stream/mp4/#{filename}/master.m3u8"
  output_pathname = File.join(current_dir, "#{@metadata[:title_jp]}#{@ext}")
  if downloaded?(output_pathname)
    puts "ダウンロード済み"
    return
  end
  metadata_array = [""].concat(%W[
    album="#{@metadata[:subject]}"
    title="#{@metadata[:title_jp]}"
    genre="#{@metadata[:genre]}"
    artist="#{@metadata[:create]}"
    date="#{@metadata[:year]}"
  ])
  metadata_options = metadata_array.join(" -metadata ")
  ffmpeg_command = %Q[ffmpeg -y -i #{master_m3u8} #{@audio_output_option} #{metadata_options} -id3v2_version 3 "#{output_pathname}" 2>&1]
  ffmpeg_command = ffmpeg_command.tosjis if @is_win
  puts "ダウンロード中..."
  download_by_ffmpeg(ffmpeg_command)
end

def download_by_ffmpeg(ffmpeg_command)
  IO.popen(ffmpeg_command) do |pipe|
    duration = nil
    progress = 0
    counter_str = '(\d\d):(\d\d):(\d\d).(\d\d)'
    pipe.each_line("r") do |line_raw|
      line = @is_win ? line_raw.tosjis : line_raw
      counter_duration_match = line.scan(/: #{counter_str},/).flatten
      unless counter_duration_match.empty?
        duration = time_counter_to_sec(counter_duration_match)
      end
      counter_progress_match = line.scan(/time=#{counter_str}/).flatten
      unless counter_progress_match.empty?
        progress = time_counter_to_sec(counter_progress_match)
        progress = duration if duration && progress > duration
      end
      if duration
        progress_bar(progress, duration, "秒")
      end
    end
    puts
  end
end

def time_counter_to_sec(counter)
  ((counter[0].to_i * 360000 + counter[1].to_i * 6000 + counter[2].to_i * 100 + counter[3].to_i) / 100.0).round
end

def progress_bar(progress, max, unit)
  max_digits = max.to_s.size
  print "\r" + "[#{'%-50s' % ('#' * (progress.to_f / max * 50).to_i)}] #{'%*d' % [max_digits, progress]} / #{max} #{unit}"
end

def current_dir
  @pwd
end

def push_dir(dir)
  @pwds ||= []
  @pwds.push(dir)
  @pwd = @pwds.last
  Dir::chdir(@pwd)
  @pwd
end

def pop_dir
  @pwds.pop
  @pwd = @pwds.last
  Dir::chdir(@pwd)
  @pwd
end

def gogaku_on_demand
  @is_win = is_win?
  unless check_ffmpeg_has_(/openssl|gnutls/)
    puts <<-EOB
gogakuondemand.rbを実行するには
OpenSSLまたはGnuTLSに対応しているFFmpegが必要です。
インストール済みのFFmpegを確認してください。
    EOB
    return
  end
  load_pref
  if @ext == '.ogg'
    unless check_ffmpeg_has_(/libvorbis/)
      puts <<-EOB
Ogg Vorbisフォーマットで保存するには
libvorbisライブラリに対応しているFFmpegが必要です。
インストール済みのFFmpegを確認してください。
      EOB
      return
    end
  end
  print_download_subjects
  data_hash = get_source_urls
  data_hash.each do |subject, date_filenames|
    @subject = subject
    subject_dir = prepare_download_each_subject(date_filenames)
    push_dir(subject_dir)
    each_date_process(date_filenames)
    pop_dir
  end
  puts "作業終了"
end

gogaku_on_demand
